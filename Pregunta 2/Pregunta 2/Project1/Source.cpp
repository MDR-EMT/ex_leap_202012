#include <iostream>

using namespace std;

class Rectangle {
public:
	float cposX;
	float cposY;
	float width;
	float height;

	Rectangle(float _cposX, float _cposY, float _width, float _height) {
		cposX = _cposX;
		cposX = _cposY;
		width = _width;
		height = _height;
	}
private:

};

void collision(Rectangle A, Rectangle B, Rectangle C) {
	cout << "A collides with B?\n";
	if ((A.cposX + A.width/2) >= (B.cposX - B.width/2) && // a la derecha de A
		(A.cposX - A.width/2) <= (B.cposX + B.width/2) && // a la izquierda de A
		(A.cposY + A.height/2) >= (B.cposY - B.height/2) && // arriba de A
		(A.cposY - A.height/2) <= (B.cposY + B.height/2)) // debajo de A
	{
		cout << "A esta en collision con B\n" << endl;
	}else {
		cout << "A no esta en collision con B\n" << endl;
	}

	cout << "A collides with C?\n";
	if ((A.cposX + A.width / 2) >= (C.cposX - C.width / 2) &&
		(A.cposX - A.width / 2) <= (C.cposX + C.width / 2) &&
		(A.cposY + A.height / 2) >= (C.cposY - C.height / 2) &&
		(A.cposY - A.height / 2) <= (C.cposY + C.height / 2)) 
	{
		cout << "A esta en collision con C\n" << endl;
	}
	else {
		cout << "A no esta en collision con C\n" << endl;;
	}
	
	cout << "B collides with C?\n";
	if ((B.cposX + B.width / 2) >= (C.cposX - C.width / 2) &&
		(B.cposX - B.width / 2) <= (C.cposX + C.width / 2) &&
		(B.cposY + B.height / 2) >= (C.cposY - C.height / 2) &&
		(B.cposY - B.height / 2) <= (C.cposY + C.height / 2)) 
	{ 
		cout << "B esta en collision con C\n" << endl;
	}
	else {
		cout << "B no esta en collision con C\n" << endl;
	}
}


int main() {
	Rectangle A(0, 0, 5, 7);
	Rectangle B(10, 10, 5, 7);
	Rectangle C(5, 0, 6, 6);

	collision(A, B, C);

	return 0;
}
